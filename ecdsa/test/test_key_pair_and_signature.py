#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric.utils import encode_dss_signature

from icecream import ic

if __name__ == '__main__':

    private_key = ec.derive_private_key(\
                        123456789012345678901234567890,
                        ec.BrainpoolP256R1())

    pn = private_key.public_key().public_numbers()
    ic(pn.x)
    ic(pn.y)

    #pub2 = ec.EllipticCurvePublicNumbers(
    #            x=0x5c83d1c561ecfcc14fdb305da1fa3f01bd2fdb9326dad8d62cc9a1e076ddcf1e,
    #            y=0x2bd0e7c8c5e429180f8f7e65cd6ca74877b48cf50c557bd37f41a409b85fb98a,
    #            curve=ec.BrainpoolP256R1()
    #)

    public_key = private_key.public_key()
    r = 64084392040251794258500776918971472902743350645397241278592827408282571573954
    s = 62777749707129314016965419046274781508294477955400549316581524401703166594839
    print("Ich prüfe die ECDSA-Signatur (r,s)=\n", r, "\n", s)

    der_encoded_signature = encode_dss_signature(r,s)
    try:
        public_key.verify(der_encoded_signature, \
                            b'Hallo Welt', \
                            ec.ECDSA(hashes.SHA256()))
    except InvalidSignature:
        print("sigfail")
    except Exception as e:
        print("Fehler", str(e))
    else:
        print("Hurra Signatur OK")





