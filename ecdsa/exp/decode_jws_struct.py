#! /usr/bin/env python3
# -*- coding: UTF-8 -*-


import base64, hashlib, json

from cryptography import x509
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives.serialization import load_pem_public_key
from cryptography.hazmat.primitives.asymmetric.utils import encode_dss_signature
from cryptography.exceptions import InvalidSignature

from datetime import datetime, timedelta

# für pretty-Printing
from icecream import ic

# Das Folgende sind nur Hilfsfunktionen damit der
# Code leicht lesbar wird.
def b64ue(data: bytes)-> str:
    return base64.urlsafe_b64encode(data).decode()

def b64ud(data: bytes)-> str:
    return base64.urlsafe_b64decode(data).decode()

def jb64d(data):
    """
    Adds back in the required padding before decoding.
    """
    padding_len = 4 - (len(data) % 4)
    data = data + ("=" * padding_len)
    return base64.urlsafe_b64decode(data)


#
# Los geht's
#

if __name__ == '__main__':

    with open("3.jwt", "rt") as f:
       my_jwt = f.read()
       # ggf. \n am Ende entfernen
       my_jwt = my_jwt.rstrip()

    (e_header, e_body, e_trailer) = my_jwt.split(".")

    # im Produktiv-Code muss die nächste Zeile 
    # (Base64-Dekodierung + JSON parsing) in einer try-Umgebung
    # laufen.
    header = json.loads(jb64d(e_header))
    ic(header)

    body = json.loads(jb64d(e_body))
    ic(body)

    assert header["alg"] in ["ES256", 'BP256R1'];

    ic(len(e_trailer), e_trailer)

    # 256-Bit-ECDSA-Signatur 
    assert len(e_trailer) == 86

    signing_cert_der = base64.b64decode(header["x5c"][0])

    cert = x509.load_der_x509_certificate(signing_cert_der)
    ic("Zertifikatsprüfung ... lasse ich hier im Beispiel aus")

    for subject_element in cert.subject:
         if subject_element.oid.dotted_string == '2.5.4.3':
             cn = subject_element.value
    assert cn
    ic("SMC-B ist von:", cn)

    signer_public_key = cert.public_key()
    pn = signer_public_key.public_numbers()
    ic(hex(pn.x))
    ic(hex(pn.y))

    raw_signature = jb64d(e_trailer)
    assert len(raw_signature) == 64
    r = int.from_bytes(raw_signature[0:32], byteorder='big', signed=False)
    s = int.from_bytes(raw_signature[32:], byteorder='big', signed=False)

    der_encoded_signature = encode_dss_signature(r,s)
    dtbs = e_header.encode() + b'.' + e_body.encode()
    my_h = hashlib.sha256(dtbs).hexdigest()
    ic(my_h)

    try:
        signer_public_key.verify(der_encoded_signature, \
                                 dtbs, \
                                 ec.ECDSA(hashes.SHA256()))
    except InvalidSignature:
        print("sigfail")
    except Exception as e:
        print("Fehler", str(e))
    else:
        print("Hurra Signatur OK")

