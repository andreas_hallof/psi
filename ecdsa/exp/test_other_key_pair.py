#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import base64, hashlib, json, sys

from cryptography import x509
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives.serialization import load_pem_private_key
from cryptography.hazmat.primitives.asymmetric.utils import encode_dss_signature
from cryptography.exceptions import InvalidSignature

from datetime import datetime, timedelta

# für pretty-Printing
from icecream import ic

if __name__ == '__main__':

    #with open("hba_aut_e256.key", "rb") as f:
    #    pem_data = f.read()

    #private_key = load_pem_private_key(pem_data, password=b'')

    private_key = ec.derive_private_key(\
                        0x1cd5ad47be9609d9f8bc93ebf887e607ac69dbe96b3281f017f709bdb121975b,
                        ec.BrainpoolP256R1())


    pn = private_key.public_key().public_numbers()
    ic(hex(pn.x))
    ic(hex(pn.y))
    print("""
    #                04:74:42:36:f2:67:c2:1a:bd:c5:3a:34:72:f9:ad:
    #                c0:03:b1:ab:16:9e:8e:87:42:b9:14:99:ff:38:6e:
    #                a7:5b:2b:0b:89:16:a8:ec:85:65:a3:02:dd:de:4c:
    #                cb:35:ad:a8:6a:98:10:e2:5d:02:c2:09:54:e5:e2:
    #                7c:ad:b6:64:48
          """)

    public_key = private_key.public_key()

    with 

    r = 64084392040251794258500776918971472902743350645397241278592827408282571573954
    s = 62777749707129314016965419046274781508294477955400549316581524401703166594839
    print("Ich prüfe die ECDSA-Signatur (r,s)=\n", r, "\n", s)

    der_encoded_signature = encode_dss_signature(r,s)
    try:
        public_key.verify(der_encoded_signature, \
                            b'Hallo Welt', \
                            ec.ECDSA(hashes.SHA256()))
    except InvalidSignature:
        print("sigfail")
    except Exception as e:
        print("Fehler", str(e))
    else:
        print("Hurra Signatur OK")





