import { createHash, randomBytes } from 'crypto';

const curve = {
	// RFC-5639, BrainpoolP256r1
	name: "brainpoolP256r1",
	// Field characteristic.
	p: 0xA9FB57DBA1EEA9BC3E660A909D838D726E3BF623D52620282013481D1F6E5377n,
	// Curve coefficients.
	a: 0x7D5A0975FC2C3057EEF67530417AFFE7FB8055C126DC5C6CE94A4B44F330B5D9n,
	b: 0x26DC5C6CE94A4B44F330B5D9BBD77CBF958416295CF7E1CE6BCCDC18FF8C07B6n,
	// Base point.
	gx: 0x8BD2AEB9CB7E57CB2C4B482FFC81B7AFB9DE27E1E3BD23C23A4453BD9ACE3262n,
	gy: 0x547EF835C3DAC4FD97F8461A14611DC9C27745132DED8E545C1D54C72F046997n,
	// Subgroup order.
	n: 0xA9FB57DBA1EEA9BC3E660A909D838D718C397AA3B561A6F7901E0E82974856A7n,
	// Subgroup cofactor.
	h: 1n
}

// Modular arithmetic ########################################################

function real_mod(k,p) {
	// gibt x=k mod p wieder, wobei das 0<=x<p gilt
	
	let tmp=k % p;
	while (tmp<0) tmp=p+tmp;

	return tmp;
}

function inverse_mod(k, p) {
	/* Returns the inverse of k modulo p.

	This function returns the only integer x such that (x * k) % p == 1.

	k must be non-zero and p must be a prime.
	*/
	if (k == 0n) {
		throw new Error('Invalid dividend / division by zero ');
	}

	if (k < 0n) {
	    //  k ** -1 = p - (-k) ** -1  (mod p)
	    return p - inverse_mod(-k, p)
	}

	// Extended Euclidean algorithm.
	var s     = 0n; 
	var old_s = 1n;
	var t     = 1n; 
	var old_t = 0n;
	var r     = p; 
	var old_r = k;

	while (r != 0n) {

		// hier ist ein interessanter Punkt
		// da alle objekte hier BigInt sind, ist "/" nicht
		// die "normale" Division -> gut.
		var quotient = old_r / r;
		// console.log("HH: ", quotient);

		/*
		old_r = r; r = old_r - quotient * r; 
		old_s = s; s = old_s - quotient * s;
		old_t = t; t = old_t - quotient * t;
		*/

		[r, old_r] = [old_r - quotient*r, r];
		[s, old_s] = [old_s - quotient*s, s];
		[t, old_t] = [old_t - quotient*t, t];

		// console.log("H2: ", r, s, t);
	}

	var gcd = old_r;
	var x = old_s;
	var y = old_t; 

	if (gcd != 1n) {
		 throw new Error("Assertion failed");
	}
	// console.log("H3: ", k, x, p);
	// console.log((k * x) % p);
	if (real_mod(k * x, p) != 1n) {
	 	throw new Error("Assertion failed");
	}

	// return x % p
	return real_mod(x, p);
}

// Functions that work on curve points #########################################

function is_on_curve(P) {
// Returns True if the given point lies on the elliptic curve.

   	// (x,y,z)=(0,0,1) represents the point at infinity.
        if (P.x==0 && P.y==0 && P.z==1) {
	       return true;
        }

	return (P.y*P.y - P.x*P.x*P.x - curve.a * P.x - curve.b) % curve.p == 0n;
}

function point_neg(point) {
// Returns -point.

	if (! is_on_curve(point)) {
		 throw new Error("Assertion failed");
	}

	if (point.x==0 && point.y==0 && point.z==1) {
		return point;
	}

	var result = {x: point.x, y: real_mod((-1n)*point.y, curve.p), z: 0n}

	if (! is_on_curve(result)) {
		 throw new Error("Assertion failed");
	}

	return result;
}

function point_add(point1, point2) {
// Returns the result of point1 + point2 according to the group law.

	if (! is_on_curve(point1)) {
		 throw new Error("Assertion failed");
	}
	if (! is_on_curve(point2)) {
		 throw new Error("Assertion failed");
	}

        if (point1.x==0 && point1.y==0 && point1.z==1) {
		// 0 + point2 = point2
		return point2;	
	}
        if (point2.x==0 && point2.y==0 && point2.z==1) {
		// point1 + 0 = point1
		return point1;	
	}

	var x1 = point1.x;
	var y1 = point1.y;
	var x2 = point2.x;
	var y2 = point2.y;

	if (x1 == x2 && y1 != y2) {
		// point1 + (-point1) = 0
		return {x: 0n, y: 0n, z: 1n};
	}

	if (x1 == x2) {
		// This is the case point1 == point2.
	    	var m = (3n * x1 * x1 + curve.a) * inverse_mod(2n * y1, curve.p)
	} else {
		// This is the case point1 != point2.
		var m = (y1 - y2) * inverse_mod(x1 - x2, curve.p)
	}

	var x3 = m * m - x1 - x2;
	var y3 = y1 + m * (x3 - x1);
	var result = {x: real_mod(x3, curve.p), y: real_mod((-1n)*y3, curve.p), z: 0n};

	if (! is_on_curve(result)) {
		 throw new Error("Assertion failed");
	}

	return result;
}

function scalar_mult(k, point) {
// Returns k * point computed using the double and point_add algorithm.

	if (! is_on_curve(point)) {
		 throw new Error("Assertion failed");
	}

	if ((k % curve.n == 0n) || 
              (point.x==0 && point.y==0 && point.z==1)) {
		return {x: 0n, y: 0n, z: 1n};
	 }

	if (k < 0) {
		// k * point = -k * (-point)
		return scalar_mult(-k, point_neg(point));
	}

	var result = {x: 0n, y: 0n, z: 1n};
	var addend = point;

	while (k != 0n) {
		var j = k;
		j = j & 1n;
		if (j != 0n) {
			// Add.
			result = point_add(result, addend);
		}

		// Double.
		addend = point_add(addend, addend);

		// k >>= 1
		k = k>>1n;
	}

	if (! is_on_curve(result)) {
		 throw new Error("Assertion failed");
	}

	return result;
}

// jetzt kommt verlassen wir den arithemtischen Teil und kommen 
// zu ECDSA

function bit_length(x) {
	return BigInt(x.toString(2).length);
}

function hash_message(message) {
// Returns the _truncated_ SHA-256 hash of the message.
// Die Bit-Länge des Hashwerts muss <= der Bitlänger von curve.n sein
// ggf. muss der Hashwert gekürzt werden.

	var message_hash_hex = createHash('sha256').update(message).digest('hex');
	// console.log("HHM0: ", message_hash_hex);
	var plus_prefix = "0x" + message_hash_hex
	// console.log("HHM: ", plus_prefix);
	var e = BigInt(plus_prefix);
	// e = int.from_bytes(message_hash, 'big')

	// FIPS 180 says that when a hash needs to be truncated, the rightmost bits
	// should be discarded.
	var len_e = bit_length(e);
	var len_n = bit_length(curve.n);
	// console.log("HHM1: ", len_e);
	// console.log("HHM2: ", len_n);

	if (len_e > len_n) {
		var z = e >> (bit_length(e) - bit_length(curve.n));
	} else {
		var z = e;
	}

	if (! (bit_length(z) <= bit_length(curve.n))) {
		 throw new Error("Assertion failed");
	}

	return z;
}

function genRandomNumber(byteCount) {
	// https://stackoverflow.com/questions/22715110/random-big-integer-in-range-javascript
	return BigInt('0x' + randomBytes(byteCount).toString('hex'));
}


function sign_message(private_key, message) {
	var z = hash_message(message);

	const Basis_Punkt = {x: curve.gx, y: curve.gy, z: 0n};

	var r = 0n;
	var s = 0n;

	while (r == 0n || s == 0n) {
		var k = curve.n;
		while (k >= curve.n) {
			k = genRandomNumber(Number(bit_length(curve.n)) >> 3);
		}
		
		var my_point = scalar_mult(k, Basis_Punkt);

		r = real_mod(my_point.x, curve.n);
		s = real_mod((z + r * private_key) * inverse_mod(k, curve.n), curve.n);
	}

	return  {r: r, s: s};
}

function verify_signature(public_key, message, signature) {
	var z = hash_message(message);

	const Basis_Punkt = {x: curve.gx, y: curve.gy, z: 0n};

	var w = inverse_mod(signature.s, curve.n);
	var u1 = real_mod(z * w, curve.n);
	var u2 = real_mod(signature.r * w, curve.n);

	var tmp_point = point_add(scalar_mult(u1, Basis_Punkt),
			 scalar_mult(u2, public_key));

	if (real_mod(signature.r, curve.n) == real_mod(tmp_point.x, curve.n)) {
	    return true;
	} else {
	    return false;
	}
}

// Convert a hex string to a byte array
function hexToBytes(hex) {
	let bytes = [];
	for (let c = 0; c < hex.length; c += 2)
		bytes.push(parseInt(hex.substr(c, 2), 16));
	return bytes;
}

// Convert a byte array to a hex string
function bytesToHex(bytes) {
	let hex = [];
	for (let i = 0; i < bytes.length; i++) {
	    let current = bytes[i] < 0 ? bytes[i] + 256 : bytes[i];
	    hex.push((current >>> 4).toString(16));
	    hex.push((current & 0xF).toString(16));
	}
	return hex.join("");
}

var P = {x: curve.gx, y: curve.gy, z: 0n}

/*
console.log("Hallo Test", curve.p % 3n);
console.log("Hallo Test", curve.p);
console.log("Hallo Test", is_on_curve(P));

console.log(P);

console.log(inverse_mod(21n,11n));
console.log(point_neg(P));

console.log(point_add(P, P));

console.log(scalar_mult(2n, P));

console.log(hash_message("Hallo Welt"));
*/

var priv_key = 123456789012345678901234567890n;
// sanity-check nur um sicher zu gehen
if (priv_key > curve.n) {
	 throw new Error("Kein gültiger privater Schlüssel");
}
console.log("Mein privater Schlüssel ist ", priv_key);
var public_key = scalar_mult(priv_key, P);
console.log("Der öffentlicher Schlüssel ist deshalb:\n", public_key);

var signature = sign_message(priv_key, "Hallo Welt");
console.log("Randomisierte ECDSA-Signatur für DTBS='Hallo Welt'\n",
	signature);

console.log("Signaturprüfung: ",
	verify_signature(public_key, "Hallo Welt", signature));


console.log("JWS Kodierung der Signatur:");
var my_sig = signature.r.toString(16).padStart(64,"0") +
           signature.s.toString(16).padStart(64,"0");
console.log(my_sig.length, " ", my_sig);

var my_sig2 = hexToBytes(my_sig);
console.log(my_sig2.length);

