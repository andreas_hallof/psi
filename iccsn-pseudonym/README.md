# Beispielverfahren 

Einer zentralen Stelle liegen die Klartextdaten (Logfiles) vor. Die zentrale
Stelle besitzt ein Pseudonymisierungsgeheimnis (256 Bit Entropie). 

Im Verfahren 2 wird eine HKDF gemäß [RFC-5869](https://datatracker.ietf.org/doc/html/rfc5869)
verwendet (state-of-the-art).

Für mehr Informationen siehe [Quelltext](example1.py)


    $ ./example1.py
    Verfahren 1: 0123456789abcdefghij wird zu f68a2d77c42c073810b68aae2acf849d712f8fd9327c933180be7604248523a1
    wenn man nicht zu viele Daten hat (Geburtstagsparadoxon), kann man kürzen auf f68a2d77c42c073810b68aae2acf849d
    Verfahren 2: 0123456789abcdefghij wird zu 3f0a323de873ef26be0f042ea04d472ca2db891436098bfa15acb064e41dffe0
    wenn man nicht zu viele Daten hat (Geburtstagsparadoxon), kann man kürzen auf 3f0a323de873ef26be0f042ea04d472c

