#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import hashlib

from binascii import hexlify, unhexlify

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.hkdf import HKDF


if __name__ == '__main__':

    iccsn = b"0123 4567 89ab cdef ghij".replace(b' ',b'')

    geheimes_pseudonymisierungsgeheimnis = unhexlify("e2670215de0ad491e929ecce58822ca0291042421a5fbd97f546cee539884808")

    assert len(iccsn) == 20
    assert len(geheimes_pseudonymisierungsgeheimnis) == 32 # also 256 Bit

    # Verfahren 1, mit minimalen Implementierungsaufwand, kryptographisch OK
    # aber nicht lehrbuch/state-of-the-art

    pseudonym = hashlib.sha256(geheimes_pseudonymisierungsgeheimnis + iccsn).hexdigest()

    print("Verfahren 1:", iccsn.decode(), "wird zu", pseudonym)
    print("wenn man nicht zu viele Daten hat (Geburtstagsparadoxon), kann man kürzen auf", pseudonym[:32])
    
    # Verfahren 2, Implementierungsaufwand etwas größer, 
    # kryptographisch state-of-the-art = sehr gut
    hkdf = HKDF(algorithm=hashes.SHA256(), length=32, salt=None, info=iccsn, backend=default_backend()) 
    pseudonym = hexlify(hkdf.derive(geheimes_pseudonymisierungsgeheimnis)).decode()

    print("Verfahren 2:", iccsn.decode(), "wird zu", pseudonym)
    print("wenn man nicht zu viele Daten hat (Geburtstagsparadoxon), kann man kürzen auf", pseudonym[:32])

