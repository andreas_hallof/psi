# Private Set Intersection

PoC für eine "Private Set Intersection" (PSI) für ePA 

Ein PVS möchte mit nur einer Anfrage von einem AS erfahren welche Versicherten
des PVS im AS eine ePA haben. Dabei soll das AS aber nicht erfahren können
welche Versicherten im PVS noch sind. Pure Zauberei.

Grundlage für das Verfahren ist eine Variante des ECDH-Schlüsselaustausches.

# Toy-Example

AS und PVS verwenden die gleiche H\_plus Funktion und eine ECC-Kurve. Die
Funktion H\_plus bildet einen Zeichenstring (KVNR) auf einen ECC-Punkt ab.

Beide haben einen Versicherten mit KVNR="A123456789"

=> x=37088112053077506736002401325026189635860156718155102679743973877935812873237 y=19961843334953659261407034832466258204603279228400932466684238654927330384101

AS verwendet:
as\_secret=80305972057199325351433202668409419032894224563838331207696621745787813960714

AS: a\_secret * (x,y) =
x\_P\_as=44652302733210312780224514422957046851991642302726277532224764379005675129553 y\_P\_as=110267311975588772435891648184412961021736477674329160756006149253652724972226

PVS verwendet:
pvs\_secret=39122849406521574269527619455678078245861900280632619392808640171176306545427

PVS: pcs\_secret * (x,y) =
x\_P\_pvs=101227174372707159495294649463914684557712381314565425442285157947059579935228 y\_P\_pvs=31537980896534579620579917166995073266826986646988571386998862873409229750753

PVS übergibt das an das AS und AS berechnet as\_secret * x\_P\_pvs:

x\_Q\_pvs=38806197527677816778352716485699147842141886441307251635541169850823722729780 y\_Q\_pvs=95479324850178299475266688282802004256132878319631920989235171853448376030028

Das gibt das AS an das PVS zurück.

Das PVS berechnet lokal: pvs\_secret * x\_P\_as:

x\_Q\_as=38806197527677816778352716485699147842141886441307251635541169850823722729780 y\_Q\_as=95479324850178299475266688282802004256132878319631920989235171853448376030028

Das PVS vergleicht beide Werte und stellt gleichheit fest. Das AS hat
pvs\_secret * x\_P\_as nicht und weiß daher gar nichts.

# Geschwindigkeit
In der natürlich nicht sonderlich schnell nativen Python-Implementierung.

Wenn man sich die Zahlen bei 'openssl speed' ansieht geht das in C um den
Faktor 100 schneller.

Mit einer guten Implementierung müsste man 33769 skalaren Multiplikationen pro
Sekunde schaffen. Dann noch mal etwas für die get\_point-Operation => 15\_000
pro Sekunde. Bei sechs Millionen Akten würde es damit im AS 400 Sekunden dauern
die Datenbasis (Menge S1) zu berechnen (=> ca. 6.6 Minuten).


## VM auf dem Server

o) get\_point + skalare Multiplikation korrekt großem Skalar
   1\_000 / 32 Sekunden => ca. 31 pro Sekunde

## Laptop, sagemath

o) skalare Multiplikation (P256) mit großem Skalar 
   10\_000 / 150.2 Sekunden => 66.5 pro Sekunde

Toll: https://kel.bz/post/sage-p256/

## Laptop, python

o) get\_point kann 100\_000/116.0 Sekunden => 862 Punkte pro Sekunde

o) get\_point + skalare Multiplikation (kleinem Skalar)
   100\_000 / 698 Sekunden => ca. 143 pro Sekunde

o) get\_point + skalare Multiplikation korrekt großem Skalar
   1\_000 / 51 Sekunden => ca. 19 pro Sekunde

## Laptop, gmp

siehe a.c

https://gmplib.org/manual/Headers-and-Libraries

