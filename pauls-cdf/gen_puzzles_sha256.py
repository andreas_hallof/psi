#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
gen_puzzle

"""

import hashlib, os, sys, time
from binascii import hexlify

if __name__ == '__main__':


    M = 1_000_000
    weight = [0, 10_000, 100_000, M, 10*M, 20*M, 50*M, 100*M,
              200*M, 500*M]
    for i in range(0, len(weight)):
        start = hashlib.sha256(os.urandom(256)).digest()
        tmp = start
        start_zeit = time.time()
        for j in range(0, weight[i]):
            tmp = hashlib.sha256(tmp).digest()
        ende  = tmp
        print(i, weight[i], hexlify(start), hexlify(ende), time.time()-start_zeit)

