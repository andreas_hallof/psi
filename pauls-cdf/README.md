# Delay-Function bei unsicherer Systemzeit

Pauls Login mit kryptographischer Delay-Funktion, bei Fehleingabe des
Passwortes, ausprobieren.

Ich möchte eine Passwort-Eingabe mit Fehlbedinungszähler (FBZ) haben. Dabei
möchte ich abhängig vom FBZ eine Wartezeit vor der nächsten Passwort-Eingabe
erzwingen. Die besondere Konstellation soll hierbei sein, dass ich der
Systemzeit nicht vertraue -- die Systemzeit soll vom beliebig vom Angreifer
manipulierbar sein.

Wir verwenden eine Delay-Function, die abhängig von FBZ vor dem
Passwort-Vergleich zwangsweise ausgeführt wird. Die Komplexität der
Delay-Function ist proportional zum FBZ.

Voraussetzungen:

1. Die Passwort-Abfrage und -Vergleich soll offline funktionieren. Es
   ist also kein Login-Server notwendig sein (ansonsten könnte man 
   noch coolere Sache machen ... wir beschränken uns absichtlich auf
   offline).

2. Die direkte Laufzeitumgebung meines Programms ist integer in der 
   Weise, dass der Angreifer nicht einfach per Online-Debugger Code-Blöcke
   überspringen oder beliebig manipulieren kann. 

3. Ich habe einen sicheren (lokalen) Speicher für den FBZ. Der Angreifer
   kann in unserem Angriffsmodell beliebig die Systemzeit verändern, aber
   eben nicht den Speicher für den FBZ.


Ablauf:

    +++ Start FBZ = 1
    +++ Nutzer übergibt Passwort
        Ich setzte den FBZ egal was erstmal um eins hoch.
        Ist der FBZ > 1 gibt es abhängig vom FBZ eine kryptographische
        Aufgabe zu lösen.
        Vergleich Passwort (SOLL<->IST).
        Falls gleich FBZ auf 0 zurücksetzen

## Demonstration

Initial ist der FDZ=0

    $ xxd data.fbz
    00000000: 30                                       0

Soll-Passwort lokal gespeichert:

    $ cat data.password
    123456

Gebe Passwort korrekt ein:

    $ ./passwort-abfrage.py 123456
    aktueller Fehlbedinungszähler: 0
    Ich setzte bevor ich das Passwort prüfe den FBZ um eins hoch
    Der echte Fehlbedienungszähler ist gleich 0, CDF ausführen ...
    - |#                                                   | 0 Elapsed Time: 0:00:00
    Passwort OK, setze Fehlbedinungszähler zurück

FBZ bleibt 0

    $ xxd data.fbz
    00000000: 30                                       0

Gebe Passwort einmal falsch ein 

    $ ./passwort-abfrage.py falsch
    aktueller Fehlbedinungszähler: 0
    Ich setzte bevor ich das Passwort prüfe den FBZ um eins hoch
    Der echte Fehlbedienungszähler ist gleich 0, CDF ausführen ...
    - |#                                                   | 0 Elapsed Time: 0:00:00
    Passwort ist falsch, FBZ ist ja schon am Anfang erhöht worden -> exit/Abbruch
    $ xxd data.fbz
    00000000: 31                                       1

Wichtig der FBZ wird immer super früh erhöht. Und vor dem Passwort-Vergleich
muss die delay-function kommen.

Wir geben jetzt mal ein paar mal das Passwort falsch ein und beim 8. mal richtig.

    $ ./passwort-abfrage.py falsch ; xxd data.fbz
    aktueller Fehlbedinungszähler: 1
    Ich setzte bevor ich das Passwort prüfe den FBZ um eins hoch
    Der echte Fehlbedienungszähler ist gleich 1, CDF ausführen ...
    100% (10 of 10) |#########################| Elapsed Time: 0:00:00 Time:  0:00:00
    Passwort ist falsch, FBZ ist ja schon am Anfang erhöht worden -> exit/Abbruch
    00000000: 32                                       2
    $ ./passwort-abfrage.py falsch ; xxd data.fbz
    aktueller Fehlbedinungszähler: 2
    Ich setzte bevor ich das Passwort prüfe den FBZ um eins hoch
    Der echte Fehlbedienungszähler ist gleich 2, CDF ausführen ...
    100% (20 of 20) |#########################| Elapsed Time: 0:00:01 Time:  0:00:01
    Passwort ist falsch, FBZ ist ja schon am Anfang erhöht worden -> exit/Abbruch
    00000000: 33                                       3
    $ ./passwort-abfrage.py falsch ; xxd data.fbz
    aktueller Fehlbedinungszähler: 3
    Ich setzte bevor ich das Passwort prüfe den FBZ um eins hoch
    Der echte Fehlbedienungszähler ist gleich 3, CDF ausführen ...
    100% (50 of 50) |#########################| Elapsed Time: 0:00:04 Time:  0:00:04
    Passwort ist falsch, FBZ ist ja schon am Anfang erhöht worden -> exit/Abbruch
    00000000: 34                                       4
    $ ./passwort-abfrage.py falsch ; xxd data.fbz
    aktueller Fehlbedinungszähler: 4
    Ich setzte bevor ich das Passwort prüfe den FBZ um eins hoch
    Der echte Fehlbedienungszähler ist gleich 4, CDF ausführen ...
    100% (100 of 100) |#######################| Elapsed Time: 0:00:08 Time:  0:00:08
    Passwort ist falsch, FBZ ist ja schon am Anfang erhöht worden -> exit/Abbruch
    00000000: 35                                       5
    $ ./passwort-abfrage.py falsch ; xxd data.fbz
    aktueller Fehlbedinungszähler: 5
    Ich setzte bevor ich das Passwort prüfe den FBZ um eins hoch
    Der echte Fehlbedienungszähler ist gleich 5, CDF ausführen ...
    100% (200 of 200) |#######################| Elapsed Time: 0:00:18 Time:  0:00:18
    Passwort ist falsch, FBZ ist ja schon am Anfang erhöht worden -> exit/Abbruch
    00000000: 36                                       6
    $ ./passwort-abfrage.py falsch ; xxd data.fbz
    aktueller Fehlbedinungszähler: 6
    Ich setzte bevor ich das Passwort prüfe den FBZ um eins hoch
    Der echte Fehlbedienungszähler ist gleich 6, CDF ausführen ...
    100% (500 of 500) |#######################| Elapsed Time: 0:00:46 Time:  0:00:46
    Passwort ist falsch, FBZ ist ja schon am Anfang erhöht worden -> exit/Abbruch
    00000000: 37                                       7
    $ ./passwort-abfrage.py falsch ; xxd data.fbz
    aktueller Fehlbedinungszähler: 7
    Ich setzte bevor ich das Passwort prüfe den FBZ um eins hoch
    Der echte Fehlbedienungszähler ist gleich 7, CDF ausführen ...
    100% (1000 of 1000) |#####################| Elapsed Time: 0:01:32 Time:  0:01:32
    Passwort ist falsch, FBZ ist ja schon am Anfang erhöht worden -> exit/Abbruch
    00000000: 38                                       8
    $ ./passwort-abfrage.py 123456 ; xxd data.fbz
    aktueller Fehlbedinungszähler: 8
    Ich setzte bevor ich das Passwort prüfe den FBZ um eins hoch
    Der echte Fehlbedienungszähler ist gleich 8, CDF ausführen ...
    100% (1500 of 1500) |#####################| Elapsed Time: 0:02:17 Time:  0:02:17
    Passwort OK, setze Fehlbedinungszähler zurück
    00000000: 30                                       0


## Überlegung

Evtl. sollte man die kryptographische Aufgabe durch eine "rein mathematische"
ersetzen: 50 MiB viele 64 Bit natürliche Zahlen, die jeweils ungleich 0 sind.
Diese multipiziert man paarweise und erhält jeweils einen 128 Bit Wert. Mit
diesem überschreibt man die zwei 64-Bit Ursprungswerte.

## kryptographische Aufgaben erstellen:

### AES-128

    $ ./gen_puzzles_aes128.py
    0 0 5.4836273193359375e-06
    1 10 0.8608705997467041
    2 20 1.711378812789917
    3 50 4.6265549659729
    4 100 9.012341022491455
    5 200 18.227678298950195
    6 500 62.96125555038452
    7 1000 91.0182454586029
    8 1500 136.65886044502258

### SHA-256

    $ ./gen_puzzles_sha256.py
    0 0 b'5098791307a9d7a6c8b011b244e6bdfdb9ba4d42f9e683ae5351983905c5227d' b'5098791307a9d7a6c8b011b244e6bdfdb9ba4d42f9e683ae5351983905c5227d' 4.76837158203125e-06
    1 10000 b'2b939c6341ecfcf6149bb400af3f100bde1c4ddc57d04b930f1619281d034dc2' b'408618bbce2d3076ca03f2271f0ff934be4fd1f7f308daa30364d435fed7fa0a' 0.009182214736938477
    2 100000 b'ab7112eedb96674839ba471b46650dc7d0df91e080ed6a714fa694d57a84ac9a' b'0c714874d7057d63179c801dda070dc817ef210ee77a1e174720c168d5235eb9' 0.05933880805969238
    3 1000000 b'3307c086bb22cdf43e324c39133c9361cd22df6a56f7fd428f4f9fcdcd149992' b'0c63132823997b6921ea55328f38a5a015dabb87bbad5ba496c32368fe619d5f' 0.5627110004425049
    4 10000000 b'e04608c3ef3694f8351332c688f88e7c35c6a1d9bff646c7ca429d97d45f02fb' b'22f95b26da99314e0143467536d075ae1201cf1c627ce4c621fc7356fe5c62b3' 5.963479280471802
    5 20000000 b'bc42b04701a99b3da0edb46a8e68b8dc8962fa5430aef74d911685cde8170507' b'29bcea26bcf8881e68d39c84507a0f529ec7314ed9042ed11f4e0aaf9514d8a9' 11.961275577545166
    6 50000000 b'30e6789b24e3e9fa4e072920062ffd701239f4f36ca956e60226a8b82ead6e7d' b'd9216202121fafb6290c1a423b4696dd29674e8dd39d7289324c79693af4e733' 55.03048896789551
    7 100000000 b'3801cb4fb241e6755ed26a0ef79adda34f301aa3697b584544873c9a331556b1' b'e19b4985a9446354eb167ff78f5f4818721f9dea5c9d30dce63f7724ac2059be' 157.29545164108276
    8 200000000 b'f91012a3978b742500e4ee3725d09509d9c31c9f31a3fa344df82204d4660e8b' b'be5a7406b22cf09f9e251b97b8a20c35e33d77d9e08496c5cb7fc6fe2d1ff912' 185.28410243988037
    9 500000000 b'ebd19f5794754e98783a49f5d80da4b18ae48af0335c70e1b51582950774cc9f' b'1be6738e8925c0f14cb7556c64c1fad9ee144e09bdf57968b319267ec600a381' 616.7994632720947


