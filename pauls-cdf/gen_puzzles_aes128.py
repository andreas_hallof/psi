#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
gen_puzzle

"""

import os, sys, time
from binascii import hexlify

from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes

if __name__ == '__main__':


    weight = [0, 10, 20, 50, 100, 200, 500, 1000, 1500]

    aes_key = os.urandom(16)
    for i in range(0, len(weight)):
        cipher = Cipher(algorithms.AES(aes_key), modes.CTR(os.urandom(16)))
        tmp = b' '*(1024*1024*50)
        start_zeit = time.time()
        for j in range(0, weight[i]):
            encryptor = cipher.encryptor()
            ciphertext = encryptor.update(tmp) + encryptor.finalize()
            tmp = ciphertext

        print(i, weight[i], time.time()-start_zeit)

