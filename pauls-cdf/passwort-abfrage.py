#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Pauls Login mit kryptographischer Delay-Funktion ausprobieren.

Ich möchte eine Passwort-Eingabe mit Fehlbedinungszähler (FBZ) haben. Dabei
möchte ich abhängig vom FBZ eine Wartezeit vor der nächsten Passwort-Eingabe
erzwingen. Die besondere Konstellation soll hierbei sein, dass ich der 
Systemzeit nicht vertrauen -- sie soll vom beliebig vom Angreifer manipulierbar
sein.

Wir verwenden eine Delay-Function, die abhängig von FBZ vor dem
Passwort-Vergleich zwangsweise ausgeführt wird. Dieses Komplexität der
Delay-Function ist proportional zum FBZ.

"""

import hashlib, os, sys
from binascii import hexlify
from progressbar import progressbar

from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes

def perform_cdf(fbz: int):
    
    assert fbz > 0

    print(f"Der echte Fehlbedienungszähler ist gleich {fbz-1}, CDF ausführen ...")

    weight = [0, 10, 20, 50, 100, 200, 500, 1000, 1500, 3000, 6000, 12_000]
    # hier kann man sich frei entfalten und eine Erhöhung der Penaltiy/Strafzeit
    # definieren so wie man möchte (Treppenfunktion, Exponetiell etc.).
    while len(weight)<fbz:
        weight.append(sum(weight))

    weight_index = fbz - 1 

    aes_key = os.urandom(16)
    cipher = Cipher(algorithms.AES(aes_key), modes.CTR(os.urandom(16)))
    # 50 MiB => so groß, dass es nicht mehr in der L2/3-Cache passt
    tmp = b' '*(1024*1024*50)
    for _ in progressbar(range(0, weight[weight_index])):
        encryptor = cipher.encryptor()
        ciphertext = encryptor.update(tmp) + encryptor.finalize()
        tmp = ciphertext
    
    # Bei einer Programmiersprache bei der der Compiler clever ist,
    # muss man jetzt mit tmp was machen (bspw. in eine Datei schreiben).
    # Ansonsten optimiert einen der Compiler die obrige Schleife evtl.
    # einfach weg. 
    # Python ist nicht so clever, aber trotzdem hier als Beispiel.
    with open("tmp.delete-me", "wb") as f:
        f.write(tmp[-2:])

    return True

if __name__ == '__main__':

    if len(sys.argv)>1:
        passwort_to_test = sys.argv[1]
    else:
        sys.exit("Als erstes Argument erwarte ich das Passwort.")

    FBZ_FILE = "data.fbz"
    if not os.path.isfile(FBZ_FILE):
        with open(FBZ_FILE, "tw") as f:
            f.write("0")
    with open(FBZ_FILE, "rt") as f:
        fbz = int(f.read())
    print("aktueller Fehlbedinungszähler:", fbz)


    print("Ich setzte bevor ich das Passwort prüfe den FBZ um eins hoch.")
    fbz += 1
    with open(FBZ_FILE, "tw") as f:
        f.write(f"{fbz}")

    perform_cdf(fbz)

    with open("data.password", "rt") as f:
        passwort = f.readline().rstrip()

    k = os.urandom(16)
    if hashlib.sha256(k + passwort_to_test.encode()).digest() == \
       hashlib.sha256(k + passwort.encode()).digest():
        print("Passwort OK, setze Fehlbedinungszähler zurück")
        with open(FBZ_FILE, "tw") as f:
            f.write("0")
    else:
        print("Passwort ist falsch, FBZ ist ja schon am Anfang erhöht worden -> exit/Abbruch.")

